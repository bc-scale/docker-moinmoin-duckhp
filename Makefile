

#all: dl_moin zip dockerbuild
all: build run


build:
	docker-compose build


run:
	docker-compose up


clean:
	rm -rf moinfiles/*
	docker container ls -a | grep "duckhp/moin-wiki:1.9.11-alpine" | cut -b 1-12 | xargs docker rm
	docker image rm duckhp/moin-wiki:1.9.11-alpine


zip:
	echo "Zipping the '/moindir.zip'.."
	zip -Z bzip2 -9 -db -XTr moindir.zip moin


dl_moin:
	wget --progress=dot -c -nc -O moin-1.9.11.tar.gz http://static.moinmo.in/files/moin-1.9.11.tar.gz
	tar xvf moin-1.9.11.tar.gz -C moin/ --keep-old-files --strip-components=2 moin-1.9.11/wiki/

